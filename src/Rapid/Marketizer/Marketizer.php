<?php

namespace Rapid\Marketizer;

use Rapid\Marketizer\Data\MarketingData;
use Rapid\Marketizer\Exception\UrlNotFoundException;
use Rapid\Marketizer\Exception\UrlMalformedException;
use Rapid\Marketizer\Exception\InvalidChannelException;

class Marketizer
{
    /**
     * Returns a structured marketing data object by filtering
     * the supplied params and their values
     *
     * @param  string        $pages         urls of the landing, conversion, referer pages
     * @param  boolean       $sessionExists if the session already exists or not
     * @return MarketingData                marketing data created by processing the urls
     */
    public function getMarketingData($pages = array(), $sessionExists = false)
    {
        $landingPage    = false;
        $conversionPage = false;
        $refererPage    = false;
        $data           = new MarketingData();

        if (isset($pages['landing']) && !empty($pages['landing'])) {
            $landingPage = true;
        }

        if (isset($pages['conversion']) && !empty($pages['conversion'])) {
            $conversionPage = true;
        }

        if (isset($pages['referer']) && !empty($pages['referer'])) {
            $refererPage = true;
        }

        if (!$landingPage && !$conversionPage) {
            throw new UrlNotFoundException();
        }

        if ($landingPage) {
            // breakdown url string into parts and organize in an array format
            // separating hostname, port, query and etc
            $landingPageParts = parse_url($pages['landing']);

            // check whether the landing page url is set
            if (isset($landingPageParts['host'])) {
                $data->setLandingPage($pages['landing']);
            }

            $socialMediaOptions  = MarketingData::$socialMediaOptions;
            $searchEngineOptions = MarketingData::$searchEngineOptions;
            $campaignOptions     = MarketingData::$campaignOptions;

            // check whether query params exists from the landing page url
            // and set campaign information accordingly
            if (isset($landingPageParts['query'])) {

                // breakdown query string into parts and organize in an array format
                // separating key value pairs in an associative array
                parse_str($landingPageParts['query'], $queryParts);

                if (count($queryParts) > 0) {
                    foreach ($queryParts as $key => $value) {
                        // check if any parameter pair matches an existing
                        // marketing campaign condition and if so, set campaign
                        if (array_key_exists($key, $campaignOptions) && $value) {
                            $data->setCampaign($value);
                            $data->setSource($campaignOptions[$key]);
                        }
                    }
                }

                // if a campaign is provided, set the channel to paid
                if ($data->getCampaign()) {
                    $data->setChannel(MarketingData::CHANNEL_PAID);
                }
            }

            if ($refererPage) {
                // breakdown url string into parts and organize in an array format
                // separating hostname, port, query and etc
                $refererParts = parse_url($pages['referer']);

                // check if the referer url is set
                if (isset($refererParts['host'])) {
                    // if a channel is not already set from campaigns
                    if (! $data->getChannel()) {
                        // check whether the referer is from a social media url
                        $this->matchUrlAndSetChannel($socialMediaOptions, $refererParts['host'], $data, MarketingData::CHANNEL_SOCIAL);
                    }

                    // if a channel is not already set from social media
                    if (! $data->getChannel()) {
                        if (! $sessionExists) {
                            // check whether the referer is from a search engine url
                            $this->matchUrlAndSetChannel($searchEngineOptions, $refererParts['host'], $data, MarketingData::CHANNEL_ORGANIC);
                        }
                    }

                    if (! $data->getChannel()) {
                        if ($refererParts['host'] == $landingPageParts['host']) {
                            if (! $sessionExists) {
                                $data->setChannel(MarketingData::CHANNEL_DIRECT);
                                $data->setSource(MarketingData::CHANNEL_DIRECT);
                            }
                        }
                    }

                    $data->setReferer($pages['referer']);
                } else {
                    // if referer url is not set, set the channel to direct since the request
                    // has reached the landing page directly without a referer
                    if (! $data->getChannel()) {
                        if (! $sessionExists) {
                            $data->setChannel(MarketingData::CHANNEL_DIRECT);
                            $data->setSource(MarketingData::CHANNEL_DIRECT);
                        }
                    }
                }
            } else {
                if (! $data->getChannel()) {
                    if (! $sessionExists) {
                        $data->setChannel(MarketingData::CHANNEL_DIRECT);
                        $data->setSource(MarketingData::CHANNEL_DIRECT);
                    }
                }
            }
        }

        if ($conversionPage && $sessionExists) {
            $data->setConversionPage($pages['conversion']);
        }

        return $data;
    }

    /**
     * Match a given url with given options and set the marketing data
     * accordingly to the MarketingData object
     * @param  array         $options set of options
     * @param  string        $url     fully qualified url
     * @param  MarketingData $data    marketing data object
     * @param  string        $channel channel type (defined in MarketingData constants)
     * @return void
     */
    private function matchUrlAndSetChannel($options = array(), $url, MarketingData &$data, $channel)
    {
        if (count($options) > 0) {
            foreach ($options as $option) {
                $matched = preg_match("/" . $option . "/i", $url);

                if ($matched == 1) {
                    $data->setChannel($channel);
                    $data->setSource($option);
                }
            }
        }
    }
}
