<?php

namespace Rapid\Tests\Marketizer;

use Rapid\Tests\TestCase;
use Rapid\Marketizer\Marketizer;
use Rapid\Marketizer\Data\MarketingData;

class MarketingDataTest extends TestCase
{
    public function testMarketingConstructor()
    {
        $this->assertInstanceOf('Rapid\Marketizer\Marketizer', new Marketizer());
    }

    public function testAdditionalInfo()
    {
        $pages = [
            "landing" => "http://gorapid.com.au/",
        ];

        $userAgent     = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
        $remoteAddress = "192.168.1.1";

        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($pages);

        $data->setUserAgent($userAgent);
        $data->setRemoteAddress($remoteAddress);

        $this->assertEquals($data->getUserAgent(), $userAgent);
        $this->assertEquals($data->getRemoteAddress(), $remoteAddress);
    }

    public function testConversionPageForExistingSession()
    {
        $pages = [
            "conversion" => "http://gorapid.com.au/finance/",
        ];

        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($pages, true);

        $this->assertEquals($data->getConversionPage(), $pages['conversion']);
        $this->assertEquals($data->getLandingPage(), null);
        $this->assertEquals($data->getReferer(), null);
    }

    public function testConversionPageForNonExistingSession()
    {
        $pages = [
            "conversion" => "http://gorapid.com.au/finance/",
        ];

        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($pages, false);

        $this->assertEquals($data->getConversionPage(), null);
        $this->assertEquals($data->getLandingPage(), null);
        $this->assertEquals($data->getReferer(), null);
    }

    /**
     * @expectedException Rapid\Marketizer\Exception\UrlNotFoundException
     * @expectedExceptionMessage Url not found
     */
    public function testNullLandingPage()
    {
        $this->marketizer = new Marketizer();
        $this->marketizer->getMarketingData(null, null);
    }

    /**
     * @dataProvider channelDataProvider
     */
    public function testChannelAndSource($pages = array(), $channel = null, $source = null, $campaign = null, $sessionExists = false)
    {
        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($pages, $sessionExists);

        $this->assertInstanceOf('Rapid\Marketizer\Data\MarketingData', $data);
        $this->assertEquals($channel, $data->getChannel());
        $this->assertEquals($source, $data->getSource());
        $this->assertEquals($campaign, $data->getCampaign());
        $this->assertEquals($pages['landing'], $data->getLandingPage());
    }

    public function channelDataProvider()
    {
        return [
            [
                [
                    'landing' => 'http://gorapid.com.au/?utm_campaign=CarSale',
                    'referer' => 'http://www.hello.com',
                ],
                MarketingData::CHANNEL_PAID,
                'facebook',
                'CarSale'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au/?campaign=adwords',
                    'referer' => 'http://www.hello.com',
                ],
                MarketingData::CHANNEL_PAID,
                'adwords',
                'adwords'
            ],
            [
                [
                    'landing' => 'https://gorapid.com.au/resources/customer-reviews/?campaign=RNA',
                ],
                MarketingData::CHANNEL_PAID,
                'adwords',
                'RNA',
            ],
            [
                [
                    'landing' => 'https://gorapid.com.au/resources/customer-reviews/?campaign=RNA',
                ],
                MarketingData::CHANNEL_PAID,
                'adwords',
                'RNA',
                false
            ],
            [
                [
                    'landing' => 'https://gorapid.com.au/resources/customer-reviews/?campaign=RNA',
                ],
                MarketingData::CHANNEL_PAID,
                'adwords',
                'RNA',
                true
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.google.com/something/asdasd',
                ],
                MarketingData::CHANNEL_ORGANIC,
                'google'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.google.com.au/something/asdasd',
                ],
                MarketingData::CHANNEL_ORGANIC,
                'google'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.bing.com.au/something/asdasd',
                ],
                MarketingData::CHANNEL_ORGANIC,
                'bing'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.ask.com/',
                ],
                MarketingData::CHANNEL_ORGANIC,
                'ask'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.aol.com/',
                ],
                MarketingData::CHANNEL_ORGANIC,
                'aol'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://plus.google.com/123123123',
                ],
                MarketingData::CHANNEL_SOCIAL,
                'plus.google'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.m.facebook.com/profile?1123123',
                ],
                MarketingData::CHANNEL_SOCIAL,
                'facebook'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.facebook.com/profile?1123123',
                ],
                MarketingData::CHANNEL_SOCIAL,
                'facebook'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.instagram.com/profile?1123123',
                ],
                MarketingData::CHANNEL_SOCIAL,
                'instagram'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.facebook.com/google?1123123',
                ],
                MarketingData::CHANNEL_SOCIAL,
                'facebook'
            ],
            // update marketing with existing session
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.facebook.com/google?1123123',
                ],
                MarketingData::CHANNEL_SOCIAL,
                'facebook',
                null,
                true
            ],
            // create initial marketing
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.facebook.com/google?1123123',
                ],
                MarketingData::CHANNEL_SOCIAL,
                'facebook',
                null,
                false
            ],
            // update marketing with existing session should expect no update for internal
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://gorapid.com.au/landingpage',
                ],
                null,
                null,
                null,
                true,
            ],
            // update with new session, should update marketing even internal
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://gorapid.com.au',
                ],
                MarketingData::CHANNEL_DIRECT,
                'direct',
                null,
                false,
            ],
            // update with new session, should update marketing even internal
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://gorapid.com.au/landingpage',
                ],
                MarketingData::CHANNEL_DIRECT,
                'direct',
                null,
                false,
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'http://www.google.com/facebook?instagram=facebook',
                ],
                MarketingData::CHANNEL_ORGANIC,
                'google'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au/google/?instagram=facebook',
                ],
                MarketingData::CHANNEL_DIRECT,
                'direct'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'invalidurl',
                ],
                MarketingData::CHANNEL_DIRECT,
                'direct'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                    'referer' => 'www.invalidurl.com',
                ],
                MarketingData::CHANNEL_DIRECT,
                'direct'
            ],
            [
                [
                    'landing' => 'http://gorapid.com.au',
                ],
                MarketingData::CHANNEL_DIRECT,
                'direct'
            ],
        ];
    }

    /**
     * @dataProvider validUrlComboDataProvider
     */
    public function testValidUrlCombo($pages = array())
    {
        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($pages);

        $this->assertInstanceOf('Rapid\Marketizer\Data\MarketingData', $data);
    }

    public function validUrlComboDataProvider()
    {
        return [
            [['landing' => 'http://gorapid.com.au']],
            [['landing' => 'http://www.gorapid.com.au']],
            [['landing' => 'http://gorapid.com.au/finance/']],
            [['landing' => 'http://gorapid.com.au/finance/?type=company']],
            [['landing' => 'http://mobile.gorapid.com.au']],
            [['landing' => 'http://gorapid.com.au', 'referer' => 'http://www.google.com']],
            [['landing' => 'http://gorapid.com.au', 'referer' => 'http://m.facebook.com']],
            [['landing' => 'http://gorapid.com.au', 'referer' => 'http://m.facebook.com/profile/123123']],
            [['landing' => 'http://gorapid.com.au', 'referer' => 'http://m.facebook.com/profile/123123?marketing=true']],
        ];
    }

    /**
     * @dataProvider missingLandingPageDataProvider
     * @expectedException Rapid\Marketizer\Exception\UrlNotFoundException
     * @expectedExceptionMessage Url not found
     */
    public function testMissingLandingPage($pages = array())
    {
        $this->marketizer = new Marketizer();
        $this->marketizer->getMarketingData($pages);
    }

    public function missingLandingPageDataProvider()
    {
        return [
            [['landing' => '']],
        ];
    }

    /**
     * @dataProvider invalidLandingPageDataProvider
     */
    public function testInvalidLandingPage($pages = array())
    {
        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($pages);

        $this->assertEquals(null, $data->getLandingPage());
    }

    public function invalidLandingPageDataProvider()
    {
        return [
            [['landing' => 'ht']],
            [['landing' => 'http://']],
            [['landing' => 'http//gorapid.com.au']],
            [['landing' => '#@%://www.gorapid.com.au']],
            [['landing' => 'gorapid.com.au/finance/']],
            [['landing' => 'http:/mobile.gorapid.com.au']],
        ];
    }

    /**
     * @dataProvider validCampaignDataProvider
     */
    public function testValidCampaignData($pages = array(), $campaign)
    {
        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($pages);

        $this->assertInstanceOf('Rapid\Marketizer\Data\MarketingData', $data);
        $this->assertEquals($campaign, $data->getCampaign());
    }

    public function validCampaignDataProvider()
    {
        $campaignOptions = MarketingData::$campaignOptions;

        return [
            [[ 'landing' => 'http://gorapid.com.au/?utm_campaign=facebook'], 'facebook'],
            [[ 'landing' => 'http://gorapid.com.au/?campaign=adwords'], 'adwords'],
            [[ 'landing' => 'https://gorapid.com.au/resources/customer-reviews/?campaign=RNA&adgroup=RNA&keyword='], 'RNA'],
            [[ 'landing' =>
                'https://gorapid.com.au/bad-credit-loans/car-finance/' .
                '?utm_source=Facebook&utm_medium=Facebook+Ads&utm_term' .
                '=Bad+Credit+Car+Loans+-+&utm_content=Bad+Credit+Car+' .
                'Loans+-+&utm_campaign=Bad+Credit+Car+Loans+-&green+hsv' .
                '+red+stripe'], 'Bad Credit Car Loans -'],
        ];
    }

    /**
     * @dataProvider invalidCampaignDataProvider
     */
    public function testInvalidCampaignData($pages = array())
    {
        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($pages);

        $this->assertInstanceOf('Rapid\Marketizer\Data\MarketingData', $data);
        $this->assertEquals(null, $data->getCampaign());
    }

    public function invalidCampaignDataProvider()
    {
        return [
            [[ 'landing' => 'https://gorapid.com.au/resources/customer-reviews/?adgroup=RNA&keyword=']],
            [[ 'landing' => 'https://gorapid.com.au/bad-credit-loans/car-finance/?utm_medium=Facebook']],
            [[ 'landing' => 'https://gorapid.com.au/bad-credit-loans/car-finance/?utm_term=Bad+Credit+Car+Loans+-']],
            [[ 'landing' => 'https://gorapid.com.au/bad-credit-loans/car-finance/?utm_content=Bad+Credit+Car+Loans+-+']],
            [[ 'landing' => 'http://gorapid.com.au/?ustm_campaign=facebook']],
            [[ 'landing' => 'http://gorapid.com.au/?campaigns=adwords']],
            [[ 'landing' => 'http://gorapid.com.au/?adwords=campaign']],
            [[ 'landing' => 'http://gorapid.com.au/?google=utm_campaign']],
            [[ 'landing' => 'http://utm-campaign.com.au']],
            [[ 'landing' => 'http://gorapid.com.au/utm-campaign']],
            [[ 'landing' => 'http://gorapid.com.au/?utm_campaign=']],
        ];
    }
}
